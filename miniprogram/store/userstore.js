// observable创建被检测的对象，对象中的属性会被转换为响应式数据
// action函数用来显式的定义action方法
import {observable,action} from 'mobx-miniprogram'
import {getStorage} from '../utils/storage'

export const userStore = observable({
  // 定义响应式数据
  token:getStorage('token') || '',
  userInfo:getStorage('userInfo') || {},

  // 定义action
  // 对 token 进行修改
  setToken: action(function (token) {
    // console.log(this);
    this.token = token
  }),
  setUserInfo:action(function(userInfo){
    this.userInfo = userInfo
  })
})