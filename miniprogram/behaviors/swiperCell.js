export const swiperCellBehavior = Behavior({
  data:{
    swiperCellQueue:[] // 用来存储单元格实例
  },
  methods:{
      // 当用户打开滑块时触发
  swiperCellOpen(e){
    const instance =  this.selectComponent(`#${e.target.id}`)
    this.data.swiperCellQueue.push(instance)
    },
    // 页面点击事件
    onSwiperCellPage(){
      this.onSwiperCellCommonClick()
    },
  
    // 点击滑块单元格时触发的事件
    onSwiperCellClick(){
      this.onSwiperCellCommonClick()
    },
  
    // 关掉滑块统一的逻辑
    onSwiperCellCommonClick(){
  // 需要对单元格实例数组进行遍历，遍历以后获取每一个实例，让每一个实例调用close方法即可
  this.data.swiperCellQueue.forEach(instance=>{
    instance.close()
  })
  // 将滑块单元格数组重置为空
  this.data.swiperCellQueue = []
  
    },
  }
})