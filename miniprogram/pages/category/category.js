import  {reqCategoryData} from '../../api/category'
Page({
  // 初始化数据
  data:{
    categoryList:[],
    activeIndex:0
  },

  updateActive(e){
    // console.log(e);
    const {index} =e.currentTarget.dataset
    this.setData({
      activeIndex:index
    })
  },

 // 获取分类数据
 async categoryList(){
  const res = await reqCategoryData()
  // console.log(res);
 if(res.code===200){
   this.setData({
     categoryList:res.data
   })
 }
  },

  // 监听页面加载
  onLoad(){
    this.categoryList()
  }
})
