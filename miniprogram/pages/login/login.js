// pages/login/login.js
import {toast} from '../../utils/extendApi'
import {reqLogin,reqUserInfo} from '../../api/user'
import {setStorage} from '../../utils/storage'
import {ComponentWithStore} from 'mobx-miniprogram-bindings'
import {userStore} from '../../store/userstore'
import {debounce} from 'miniprogram-licia'
import Schema from 'async-validator'
ComponentWithStore({
  storeBindings:{
    store:userStore,
    fields:['token','userInfo'],
    actions:['setToken','setUserInfo']
  },
methods:{
  login:debounce(function(){
    wx.login({
      success:async ({code}) => {
        if(code){
          const {data} = await reqLogin(code)
          // console.log(res);
          setStorage('token',data.token)

          // 将自定义登录态token存储到store对象
          this.setToken(data.token)
          this.getUserInfo()
          wx.navigateBack()
        }else {
          toast({
            title:'授权失败'
          })
        }
      },
    })
  },500),
 async getUserInfo(){
  const {data} = await reqUserInfo()
  // console.log(data);
  setStorage('userInfo',data)
  this.setUserInfo(data)
  }
}
})
