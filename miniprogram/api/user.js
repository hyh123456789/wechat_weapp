import http from '../utils/http'

export const reqLogin = (code)=>{
  return http.get(`/weixin/wxLogin/${code}`)
}

export const reqUploadFile = (filePath,name)=>{
  return http.upload('/fileUpload',filePath,name)
}

export const reqUserInfo = ()=>{
  return http.get('/weixin/getuserInfo')
}


// 更新用户信息
export const reqUpdateUserInfo = (userInfo)=>{
  return http.post('/weixin/updateUser',userInfo)
}