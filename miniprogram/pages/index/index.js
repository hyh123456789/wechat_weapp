import {reqIndexData} from '../../api/index'
Page({
  // 初始化数据
  data:{
    bannerList:[],
    categoryList:[],
    activeList:[],
    hotList:[],
    guessList:[],
    loading:true
  },
  // 获取首页数据
 async getIndexData(){
    const res = await reqIndexData()
    // console.log(res);
    this.setData({
      bannerList:res[0].data,
      categoryList:res[1].data,
      activeList:res[2].data,
      hotList:res[3].data,
      guessList:res[4].data,
      loading:false
    })
  },
  // 监听页面加载
  onLoad(){
    this.getIndexData()
  },
  onShareAppMessage(){},
  onShareTimeline(){}
})
