// pages/goods/list/index.js
import {reqGoodsList} from '../../../api/goods'
Page({
  /**
   * 页面的初始数据
   */
  data: {
    goodsList: [], // 商品列表数据
    isFinish: false, // 判断数据是否加载完毕
    total:0,
    isLoading:false, // 判断数据是否加载完毕
     // 商品列表请求参数
     requestData:{
      page:1,
      limit:10,
      category1Id:'',
      category2Id:''
    }
  },
  // 获取商品列表数据
 async getGoodsList(){
   this.data.isLoading = true
    const {data} = await reqGoodsList(this.data.requestData)
    this.data.isLoading = false
    this.setData({
      goodsList:[...this.data.goodsList,...data.records],
      total:data.total
    })
  },
  // 监听页面的下拉刷新操作
  onPullDownRefresh(){
    // 将数据进行重置
    this.setData({
      goodsList:[],
      total:0,
      isFinish:false,
      requestData:{...this.data.requestData,page:1}
    })
    // 使用最新的参数发送请求
    this.getGoodsList()
    // 手动关闭下拉刷新效果
    wx.stopPullDownRefresh()
  }, 

  // 监听页面的上拉操作
onReachBottom(){
  const {goodsList,total,requestData,isLoading} = this.data
  const {page} = requestData 
  // 如果状态等于true，说明请求正在发送中，如果请求正在发送中，就不请求下一页数据
  if(isLoading) return
  if(goodsList.length===total){
    this.setData({
      isFinish:true
    })
    return
  }
  this.setData({
    requestData:{...this.data.requestData,page:page+1}
  })
  // 重新获取数据
  this.getGoodsList()
},

  onLoad(options){
    Object.assign(this.data.requestData,options)
    this.getGoodsList()
  },
  onShareAppMessage(){},
  onShareTimeline(){}

})
