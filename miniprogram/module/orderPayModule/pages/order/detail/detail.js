import {reqOrderAddress,reqOrderInfo,reqBuyNowGoods,reqSubmitOrder,reqPreBuyInfo,reqPayStatus} from '../../../api/orderpay'

import {formatTime} from '../../../utils/formatTime'

import Schema from 'async-validator'
// 获取应用实例
const app = getApp()
Page({
  data: {
    buyName: '', // 订购人姓名
    buyPhone: '', // 订购人手机号
    deliveryDate: '', // 期望送达日期
    blessing: '', // 祝福语
    show: false, // 期望送达日期弹框
    orderAddress:{},
    orderInfo:{},
    minDate: new Date().getTime(),
    // currentDate: new Date().getTime()
  },

  // 选择期望送达日期
  onShowDateTimerPopUp() {
    this.setData({
      show: true
    })
  },

 async submitOrder(){
    const {buyName,buyPhone,deliveryDate,blessing,orderAddress,orderInfo} = this.data

    const params ={
      buyName,
      buyPhone,
      cartList:orderInfo.cartVoList,
      deliveryDate,
      remarks:blessing,
      userAddressId:orderAddress.id
    }
 const {valid} = await this.validatorPerson(params)
 if(!valid) return
 const res = await reqSubmitOrder(params)
 if(res.code===200){
   this.orderNo=res.data
   this.advancePay()
 }
  },
  // 获取预付单信息，支付参数
async  advancePay(){
try {
  const res =  await  reqPreBuyInfo(this.orderNo)
  if(res.code===200){
  const payInfo =  await wx.requestPayment(res.data)
  if(payInfo.errMsg==='requestPayment:ok'){
    const payStatus = await reqPayStatus(this.orderNo)
    if(payInfo.code===200){
      wx.redirectTo({
        url: '/module/orderPayModule/pages/order/list/list',
        success:()=>{
          wx.toast({title:'支付成功',icon:'success'})
        }
      })
    }
  }
  }
} catch (error) {
  wx.toast({
    title:'支付失败',
    icon:'error'
  })
}
  },
  validatorPerson(params){
    // 验证收货人，是否只包含大小写字母、数字和中文字符
const nameRegExp = '^[a-zA-Z\\d\\u4e00-\\u9fa5]+$'

// 验证订购人手机号，是否符合中国大陆手机号码的格式
const phoneReg = '^1(?:3\\d|4[4-9]|5[0-35-9]|6[67]|7[0-8]|8\\d|9\\d)\\d{8}$'

  // 创建验证规则，验证规则是一个对象
    // 每一项是一个验证规则，验证规则属性需要和验证的数据进行同名
    const rules = {
      userAddressId:
        { required: true, message: '请输入收货地址'},
        buyName: [
        { required: true, message: '请输入订购人姓名' },
        { pattern: nameRegExp, message: '订购人姓名不合法' }
      ],
      buyPhone: [
        { required: true, message: '请输入订购人手机号' },
        { pattern: phoneReg, message: '手机号不合法' }
      ],
      deliveryDate: { required: true, message: '请选择送达日期' },
    }
        // 创建验证实例，并传入验证规则
        const validator = new Schema(rules)
         // 调用实例方法对数据进行验证
    // 注意：我们希望将验证结果通过 Promsie 的形式返回给函数的调用者
    return new Promise((resolve)=>{
      validator.validate(params,(errors)=>{
        if(errors){
          // 如果验证失败，需要给用户进行提示
          wx.toast({
            title: errors[0].message
          })

          resolve({ valid: false })
        }else {
          resolve({ valid: true })
        }
      })
    })
  },
  // 期望送达日期确定按钮
  onConfirmTimerPicker(event) {
    const timeRes = formatTime(new Date(event.detail))
    this.setData({
      show: false,
      deliveryDate:timeRes
    })
  },

  // 期望送达日期取消按钮 以及 关闭弹框时触发
  onCancelTimePicker() {
    this.setData({
      show: false,
      minDate: new Date().getTime(),
      currentDate: new Date().getTime()
    })
  },

  // 跳转到收货地址
  toAddress() {
    wx.navigateTo({
      url: '/module/settingModule/pages/address/list/index'
    })
  },
  // 获取订单页面的收获地址
  async getAddress(){
    //  判断全局共享的address中是否存在数据
    //  如果存在数据，就需要从全局共享的address中取到数据进行赋值
    const addressId = app.globalData.address.id
    if(addressId){
      this.setData({
        orderAddress:app.globalData.address
      }) 
      return
    }
    const {data:orderAddress} = await reqOrderAddress()
    this.setData({
      orderAddress
    })
  },
  // 获取订单详情数据
  async getOrderInfo(){
    const {goodsId,blessing} = this.data
    const {data:orderInfo} =goodsId ? await reqBuyNowGoods({goodsId,blessing}) : await reqOrderInfo()
    const orderGoods = orderInfo.cartVoList.find(item=>item.blessing !=='')
    this.setData({
      orderInfo,
      blessing:!orderGoods ? '' : orderGoods.blessing
    })
  },
  onLoad(options){
    this.setData({
      ...options
    })
  },
  onShow(){
    this.getAddress()
    this.getOrderInfo()
  },
  onUnload(){
    app.globalData.adddress = {}
  }
})
