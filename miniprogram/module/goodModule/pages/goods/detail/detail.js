// pages/goods/detail/index.js
import {reqGoodsInfo} from '../../../api/goods'
import {userBehavior} from '../../../hehaviors/userBehavior'
import {reqAddCart,reqCartList} from '@/api/cart'
Page({
  behaviors:[userBehavior],
  // 页面的初始数据
  data: {
	goodsInfo: {}, // 商品详情
    show: false, // 控制加入购物车和立即购买弹框的显示
    count: 1, // 商品购买数量，默认是 1
    blessing: '', // 祝福语
    buyNow:0,
    allCount:''
  },

  // 加入购物车
  handleAddcart() {
    this.setData({
      show: true,
      buyNow:0
    })
  },

  // 立即购买
  handeGotoBuy() {
    this.setData({
      show: true,
      buyNow:1
    })
  },

  // 点击关闭弹框时触发的回调
  onClose() {
    this.setData({ show: false })
  },
  // 全屏预览图片
  previewImage(){
    wx.previewImage({
      urls: this.data.goodsInfo.detailList,
    })
  },

  // 监听是否更改了购买数量
  onChangeGoodsCount(event) {
    // console.log(event.detail)
    this.setData({
      count:Number(event.detail)
    })
  },
  // 弹窗确定按钮触发的事件处理函数
 async handlerSubmit(){
    const {token,count,blessing,buyNow} = this.data
    const goodsId = this.goodsId
    if(!token){
      wx.navigateTo({
        url: '/pages/login/login',
      })
      return
    }
    // 区分是加入购物车还是立即购买
    if(buyNow===0){
  const res =  await  reqAddCart({goodsId,count,blessing})
  if(res.code===200){
    wx.toast({title:'加入购物车成功'})
    this.getCartCount()
    this.setData({show:false})
  }
    }else {
      wx.navigateTo({
        url: `/module/orderPayModule/pages/order/detail/detail?goodsId=${goodsId}&blessing=${blessing}`,
      })
    }
  },
  // 获取商品详情的数据
  async getGoodsInfo(){
    const {data:goodsInfo} = await reqGoodsInfo(this.goodsId)
    this.setData({
      goodsInfo
    })
  },
  // 计算购物车商品的数量
  async getCartCount(){
    if(!this.data.token) return
    const res = await reqCartList()
    if(res.data.length!==0){
      let allCount =0
      res.data.forEach(item=>{
        allCount += item.count
      })
      this.setData({
        allCount:(allCount >99 ? '99+':allCount)+''
      })
    }
  },
  onLoad(optios){
    // 将id挂载到this上
    this.goodsId = optios.goodsId
    this.getGoodsInfo()
    this.getCartCount()
  },
  onShareAppMessage(){},
  onShareTimeline(){}
})
