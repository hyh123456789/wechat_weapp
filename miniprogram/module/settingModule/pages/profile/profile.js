// pages/profile/profile.js
import {userBehavior} from './behavior'
import {setStorage} from '../../../../utils/storage'
import {reqUpdateUserInfo,reqUploadFile} from '@/api/user'
import {toast} from '../../../../utils/extendApi'
Page({
  // 注册behaviors,数据将映射到data
  behaviors:[userBehavior],

  // 页面的初始数据
  data: {
    isShowPopup: false // 控制更新用户昵称的弹框显示与否
  },

 async updateUserInfo(){
const res = await reqUpdateUserInfo(this.data.userInfo)
if(res.code===200){
  setStorage('userInfo',this.data.userInfo)
  this.setUserInfo(this.data.userInfo)
  toast({
    title:'用户信息更新成功'
  })
}
  },

 async chooseAvatar(e){
    // 获取头像的临时路径
    // 临时路径具有失效时间，需要将临时的上传到服务器，获取永久路径
    const {avatarUrl} = e.detail
    // 在获取头像临时路径以后，需要将临时路径通过wx.uploadFile上传到服务器
    const res = await reqUploadFile(avatarUrl,'file')
              this.setData({
      'userInfo.headimgurl':res.data
              })
    // wx.uploadFile({
    //   filePath: avatarUrl,
    //   name: 'file',
    //   url: 'https://gmall-prod.atguigu.cn/mall-api/fileUpload',
    //   header:{
    //     token:getStorage('token')
    //   },
    //   success:(res)=>{
    //       // 调用uploadFile方法，返回的结果是JSON字符串，需要进行转换
    //       const uploadRes = JSON.parse(res.data)
    //           this.setData({
    //   'userInfo.headimgurl':uploadRes.data
    // })
    //   }
    // })

  },
  getNickName(e){
    const {nickname} = e.detail.value
    this.setData({
      'userInfo.nickname':nickname,
      isShowPopup:false
    })
  },
  // 显示修改昵称弹框
  onUpdateNickName() {
    this.setData({
      isShowPopup: true,
      'userInfo.nickname':this.data.userInfo.nickname
    })
  },

  // 弹框取消按钮
  cancelForm() {
    this.setData({
      isShowPopup: false
    })
  }
})
