// 导入封装的网络请求模块实例
import http from '../utils/http'

export const reqIndexData = ()=>{
  // 通过并发请求获取首页的数据，提升页面的渲染速度
  // 通过Promise.all进行并发请求
  // return Promise.all([
  //   http.get('/index/findBanner'),
  //   http.get('/index/findCategory1'),
  //   http.get('/index/advertisement'),
  //   http.get('/index/findListGoods'),
  //   http.get('/index/findRecommendGoods')
  // ])
  // 使用封装的all方法发送并发请求
  return http.all(
    http.get('/index/findBanner'),
    http.get('/index/findCategory1'),
    http.get('/index/advertisement'),
    http.get('/index/findListGoods'),
    http.get('/index/findRecommendGoods')
  )
}