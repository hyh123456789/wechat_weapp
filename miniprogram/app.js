App({
  // globalData是指全局共享的数据
  // 点击收获地址时，需要将点击的收获地址赋值给address
  // 在结算支付、订单结算页面，需要判断address是否存在数据
  // 如果存在数据，就展示address数据，如果没有数据，就从接口获取数据进行渲染
  globalData:{
    address:{}
  }
})
