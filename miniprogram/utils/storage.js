/**
 * @description 存储数据
 * @param{*}key 本地存储中指定的key
 * @param{*}value需要存储的数据
 * ***/

export const setStorage = (key,value)=>{  
  try {
    wx.setStorageSync(key,value)
  } catch (error) {
    console.error(error)
  }
}
export const getStorage = (key)=>{
  try {
  const value =  wx.getStorageSync(key)
  if(value){
    return value
  }
  } catch (error) {
    console.error(error)
  }
}
export const removeStorage = (key)=>{
  try {
    wx.removeStorageSync(key)
  } catch (error) {
    console.error(error)
  }
}
export const clearStorage = ()=>{
  try {
    wx.clearStorageSync()
  } catch (error) {
    console.error(error)
  }
}