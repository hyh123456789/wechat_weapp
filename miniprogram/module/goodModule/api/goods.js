import http from '../../../utils/http'

/**
 * @description 获取商品列表数据
 * @param {Object} { page,limit,category1Id,category2Id }
 */
export const reqGoodsList = ({page,limit,...params})=>{
  return http.get(`/goods/list/${page}/${limit}`,params)
}

export const reqGoodsInfo = (goodsId)=>{
  return http.get(`/goods/${goodsId}`)
}