// const toast = (options={})=>{}
/**
 * @description 消息提示框
 * @param {Object} options 参数和wx.showToast参数保持一致
 * ***/
const toast = ({title='数据加载中...',icon='none',duration=2000,mask=true}={})=>{
  wx.showToast({
    title,
    icon,
    duration,
    mask
  })
}

const model = (options={})=>{
  return new Promise((resolve)=>{
    // 默认参数
    const defaultOpt = {
      title:'提示',
      content:'您确定要执行吗',
      confirmColor:'#f3514f'
    }
  const opts =  Object.assign({},defaultOpt,options)
    wx.showModal({
      ...opts,
      complete({confirm,cancel}){
        confirm && resolve(true)
        cancel && resolve(false)
      }
    })
  })
}
wx.toast = toast
wx.model = model

export {toast,model}